const LocalStrategy = require('passport-local').Strategy;

require('dotenv').config();

class Authentication {
    constructor(passport, passportJWT, BcryptHelper, userModel) {
        this.passport = passport;
        this.passportJWT = passportJWT;
        this.bcryptHelper = BcryptHelper;
        this.userModel = userModel;
    }

    setLocalStorageStrategy() {
        return new LocalStrategy({
                usernameField: 'username',
                passwordField: 'password',
                passReqToCallback: true,
                failureFlash: true
            }, async (req, username, password, done) => {

                if (!username || !password) {
                    return done(null, false, {message: 'Missing name or password!'});
                }

                try {
                    //table name, columns, condition column, value
                    const [user, fields] = await this.userModel.findByField(this.userModel.tableName, '*', 'name', username);

                    if (!user.length) return done(null, false, {message: 'User not found!'});

                    const passwordValidation = await this.bcryptHelper.isPasswordValid(password, user[0].password);

                    if (!passwordValidation) return done(null, false, {message: 'Incorrect password!'});

                    return done(null, user[0]);
                } catch (error) {
                    return done(error);
                }
            }
        );
    }

    setJWTStrategy() {
        return new this.passportJWT.Strategy({
                jwtFromRequest: this.passportJWT.ExtractJwt.fromAuthHeaderAsBearerToken(),
                secretOrKey: process.env.JWT_SECRET
            },
            async (jwtPayload, done) => {
                try {
                    //table name, columns, condition column, value
                    const [user, fields] = await this.userModel.findByField(this.userModel.tableName, ['id', 'name'], 'id', jwtPayload.id);

                    if (!user.length) return done(null, false, {message: 'User not found!'});

                    return done(null, user[0]);
                } catch (error) {
                    return done(error);
                }
            }
        );
    }

    initialize() {
        this.passport.use('local', this.setLocalStorageStrategy());
        this.passport.use('jwt', this.setJWTStrategy());

        this.passport.serializeUser((user, done) => {
            done(null, user.id);
        });

        this.passport.deserializeUser(async (id, done) => {
            //table name, columns, condition column, value
            const [row, fields] = await this.userModel.findByField(this.userModel.tableName, ['id', 'name'], 'id', id);
            done(null, row[0]);
        });
    }
}

module.exports = Authentication;
