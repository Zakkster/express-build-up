const mysql = require('mysql2/promise');
const dbConfig = require('../configs/db');

class MysqlConnector {
    static async createPool() {
        try {
            return await mysql.createPool(dbConfig);
        } catch (error) {
            throw error;
        }
    }

    static async executeQuery(query, params = []) {
        try {
            const pool = await MysqlConnector.createPool();
            return await pool.execute(query, params);
        } catch (error) {
            throw error;
        }
    }

    //for bulk insert
    static async runQuery(query, params = []) {
        try {
            const pool = await MysqlConnector.createPool();
            return await pool.query(query, params)
        } catch (error) {
            throw error;
        }
    }
}

module.exports = MysqlConnector;