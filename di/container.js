class Container {

    constructor() {
        this._services = new Map();
        this._singletons = new Map();
    }

    register(name, definition, dependencies) {
        this._services.set(name, {definition: definition, dependencies: dependencies});
    }

    singleton(name, definition, dependencies) {
        this._services.set(name, {definition: definition, dependencies: dependencies, singleton: true});
    }

    get(name) {
        const service = this._services.get(name);

        if (this._isClass(service.definition)) {

            if (service.singleton) {
                const singletonInstance = this._singletons.get(name);

                if (singletonInstance) {
                    return singletonInstance
                }

                const newSingletonInstance = this._createInstance(service);
                this._singletons.set(name, newSingletonInstance);

                return newSingletonInstance;
            }

            return this._createInstance(service);

        }

        return service.definition;
    }

    _getResolvedDependencies(service) {
        let classDependencies = [];
        if (service.dependencies) {
            classDependencies = service.dependencies.map((dep) => {
                return this.get(dep);
            });
        }

        return classDependencies;
    }

    _createInstance(service) {
        return new service.definition(...this._getResolvedDependencies(service));
    }

    _isClass(definition) {
        if (typeof definition === "function") {
            //check if the class has static methods, effective only if the class has only static methods
            const staticMethods = Object.getOwnPropertyNames(definition)
                .filter(prop => typeof definition[prop] === "function");

            return staticMethods.length === 0;
        }

        return false;
    }
}

module.exports = Container;