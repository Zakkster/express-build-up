const Container = require('./container');

const container = new Container();

//configs
container.register('mainConfig', require('../configs/mainConfig'));
container.register('dbConfig', require('../configs/db'));

//auth
container.register('passport', require('passport'));
container.register('passport-jwt', require('passport-jwt'));
container.register('bcrypt', require('bcrypt'));

//adapters
container.singleton('dbConnector', require('../db/MysqlConnector'));
container.singleton('bookshelf', require('../db/bookshelf'), ['dbConfig']);

//helpers
container.register('bcryptHelper', require('../helpers/BcryptHelper'), ['bcrypt']);
container.register('colors', require('colors'));
container.register('errorsHelper', require('../helpers/ErrorsHelper'), ['colors']);
container.register('validationHelper', require('../helpers/validationHelper'));

//models
container.register('mysqlUserModel', require('../models/mysql/UserModel'), ['dbConnector']);

container.register('userModel', require('../models/User'), ['bookshelf', 'bcryptHelper']);
container.register('auth', require('../core/Authentication'), ['passport', 'passport-jwt', 'bcryptHelper', 'mysqlUserModel']);

module.exports = container;