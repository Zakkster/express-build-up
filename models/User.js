const bcrypt = require('bcrypt');

module.exports = class User {
    constructor(bookshelf, bcryptHelper) {
        this.tableName = 'users';
        this.bookshelf = bookshelf;

        this.bcryptHelper = bcryptHelper;

        //move in config
        this.bcryptSaltRounds = 10;

        this.setModel();
    }

    async hashPassword(password) {
        try {
            const salt = await bcrypt.genSalt(this.bcryptSaltRounds);
            return await bcrypt.hash(password, salt);
        } catch (error) {
            throw new Error('Error in encrypting password!');
        }
    }

    //model.attributes - all model fields;
    //can validate on hook saving;
    setModel() {
        const _this = this;
        
        this.model = this.bookshelf.Model.extend({
            tableName: _this.tableName,
            hasTimestamps: ['created_at'],
            initialize() {
                this.constructor.__super__.initialize.apply(this, arguments);

                this.on('fetched:collection', (model, attrs, options) => {

                });
                this.on('fetching', async (model, attrs, options) => {

                });
                this.on('fetched', async (model, attrs, options) => {

                });
                this.on('saving', async (model, attrs, options) => {
                    try {
                        const hashedPassword = await _this.bcryptHelper.hashPassword(model.get('password'));
                        model.set('password', hashedPassword);
                    } catch (error) {
                        console.log(error);
                        throw new Error('Error in saving new user!');
                    }
                });
                this.on('saved', (model, response, options) => console.log('Model saved!'));
            }
        });
    }
};