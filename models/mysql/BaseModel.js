const validation = require('../../helpers/validationHelper');
const validationMessages = require('../../helpers/validationMessages');

module.exports = class BaseModel {
    constructor(dbConnector) {
        this.dbConnector = dbConnector;

        this.validation = validation;
        this.validationMessages = validationMessages;
    }

    async findByField(model, fields = '*', field, value) {
        if (fields.length > 1) fields = fields.join();
        try {
            return await this.dbConnector.executeQuery(`SELECT ${fields} FROM ${model} WHERE ${field} = ?`, [value]);
        } catch (error) {
            throw error;
        }
    }

    async insertInto(model, fields, values) {
        if (values.length % fields.length !== 0) throw new Error('Incorrect number of columns or values!');

        const valuesSubstituted = values.map(value => '?');

        try {
            return await this.dbConnector.executeQuery(`INSERT INTO ${model} (${fields.join()}) VALUES (${valuesSubstituted.join()})`, values);
        } catch (error) {
            throw error;
        }
    }

    async bulkInsert(model, fields, values) {
        try {
            return await this.dbConnector.runQuery(`INSERT INTO ${model} (${fields.join()}) VALUES ?`, [values]);
        } catch (error) {
            throw error;
        }
    }

    async update(model, field, conditionField, values) {
        try {
            return await this.dbConnector.executeQuery(`UPDATE ${model} SET ${field} = ? WHERE ${conditionField} = ?`, values);
        } catch (error) {
            throw error;
        }
    }

    async updateMultipleFields(model, fields = [], conditionField, values) {
        const columns = fields.map(field => `${field} = ?`);

        try {
            return await this.dbConnector.executeQuery(`UPDATE ${model} SET ${columns.join()} WHERE ${conditionField} = ?`, values);
        } catch (error) {
            throw error;
        }
    }

    async deleteByField(model, field, value) {
        try {
            return await this.dbConnector.executeQuery(`DELETE FROM ${model} WHERE ${field} = ?`, [value]);
        } catch (error) {
            throw error;
        }
    }
};