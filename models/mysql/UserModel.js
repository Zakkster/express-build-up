const BaseModel = require('./BaseModel');

module.exports = class UserModel extends BaseModel {
    constructor(dbConnector) {
        super(dbConnector);

        this.tableName = 'users';
    }
};