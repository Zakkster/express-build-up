const express = require('express');
const router = express.Router();

router.get('*', function(req, res){
    res.status(404).send({error: 'Url not Found!'})
});

module.exports = router;