const express = require('express');
const router = express.Router();

require('dotenv').config();

const keyPublishable = process.env.PUBLISHABLE_KEY;
const keySecret = process.env.SECRET_KEY;

const stripe = require("stripe")(keySecret);

router.get('/charge', async (req, res) => {
    res.json({stripeToken: keyPublishable});
});

router.post('/charge', async (req, res) => {

    // hardcoded amount for test
    const amount = 2500;

    try {
        const customer = await stripe.customers.create({
            email: req.body.stripeEmail,
            card: req.body.stripeToken
        });

        const charge = await stripe.charges.create({
            amount, //request body in 100s
            description: "Sample Charge", //request body
            currency: "usd", //request body
            customer: customer.id
        });

        //log charge, db - payment done;
        return res.json({success: charge});
    } catch (error) {
        //log error
        console.log("Error:", error);
        res.json({error: 'Payment failed'});
    }
});

router.post('/refund', async (req, res) => {
    //get the charge id first:
    try {
        const refund = await stripe.refunds.create({
            // charge id hardcoded for test
            charge: "ch_ATbgYw7MpO4hlv",
            //amount: if desired a partial refund
        });

        //log refund
        res.json({success: 'Refund was successful!'})
    } catch (error) {
        console.log(error);
        //log refund error
        res.json({error: 'Refund failed!'});
    }
});

module.exports = router;