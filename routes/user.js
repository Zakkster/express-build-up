const express = require('express');
const router = express.Router();

const container = require('../di/registerClasses');
const UserModel = container.get('mysqlUserModel');

router.get('/:userId(\\d+)', async (req, res) => {
    const userId = req.params.userId;

    try {
        const [user, fields] = await UserModel.findByField(UserModel.tableName, '*', 'id', userId);

        let result = [];
        if (user.length) result = user[0];

        return res.json({success: result});

    } catch (error) {
        return res.json(error);
    }
});

router.put('/:userId(\\d+)', async (req, res) => {
    const userId = req.params.userId;
    const field = req.body.name;

    if(!UserModel.validation.isClean(field)) return res.json({error: UserModel.validationMessages.isClean(field)});

    try {
        await UserModel.update(UserModel.tableName, 'name', 'id', [field, userId]);

        return res.json({success: 'User updated successfully!'});

    } catch (error) {
        return res.json(error);
    }
});

router.delete('/:userId(\\d+)', async (req, res) => {
    const userId = req.params.userId;

    try {
        await UserModel.deleteByField(UserModel.tableName, 'id', userId);

        return res.json({success: 'User deleted successfully!'});

    } catch (error) {
        return res.json(error);
    }
});

router.get('/profile', (req, res) => {
    res.json({user: req.user});
});

module.exports = router;