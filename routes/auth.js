const express = require('express');
const router = express.Router();

const container = require('../di/registerClasses');
const UserModel = container.get('mysqlUserModel');
const bcryptHelper = container.get('bcryptHelper');

const passport = container.get('passport');
const jwt = require('jsonwebtoken');

require('dotenv').config();

router.get('/login', (req, res) => {
    res.json('login');
});

router.post('/login', (req, res, next) => {
    passport.authenticate('local', (err, user, info) => {
        if (err) return next(err);
        if (!user) return res.json({error: info.message});

        req.logIn(user, (err) => {
            if (err) return next(err);

            // can be used setToken function
            const token = jwt.sign({
                    id: user.id
                },
                process.env.JWT_SECRET, {
                    expiresIn: 60 * 60
                }
            );

            const {id, name} = user;

            return res.json({
                userId: id,
                userName: name,
                token
            });
        });
    })(req, res, next);
});

router.post('/register', async (req, res) => {
    try {
        const {username, password} = req.body;

        const [userExists, fields] = await UserModel.findByField(UserModel.tableName, '*', 'name', username);

        if (userExists.length) return res.json({error: 'User is already existing'});

        const hashedPassword = await bcryptHelper.hashPassword(password);

        const [insertedUser, insertedUserFields] = await UserModel.insertInto(UserModel.tableName, ['name', 'password'], [username, hashedPassword]);

        // can be used setToken function
        const token = jwt.sign({
                id: insertedUser.insertId
            },
            process.env.JWT_SECRET, {
                expiresIn: 60 * 60
            }
        );

        return res.json({
            success: 'User created successfully!',
            token
        });
    } catch (error) {
        throw error;
    }

});

router.get('/logout', (req, res) => {
    //req.logout();
    res.redirect('/auth/login');
});

module.exports = router;