const express = require('express');
const router = express.Router();

const user = require('./user');
const payment = require('./payment');
const others = require('./others');
const auth = require('./auth');

const container = require('../di/registerClasses');
const passport = container.get('passport');

//authorization - list routes
router.use('/auth', auth);

//user - list routes
router.use('/user', passport.authenticate('jwt', {session: false}), user);

//payment, protect with token - jwt or else, list routes
router.use('/payment', passport.authenticate('jwt', {session: false}), payment);

//route not found
router.use(others);

module.exports = router;