module.exports = class BcryptHelper {
    constructor(bcrypt) {
        this.bcrypt = bcrypt;
        this.bcryptSaltRounds = 10;
    }

    async hashPassword(password) {
        try {
            const salt = await this.bcrypt.genSalt(this.bcryptSaltRounds);
            return await this.bcrypt.hash(password, salt);
        } catch (error) {
            throw new Error('Error in encrypting password!');
        }
    }

    async isPasswordValid(password, hash) {
        try {
            return await this.bcrypt.compare(password, hash);
        } catch (error) {
            throw error;
        }

    }
};