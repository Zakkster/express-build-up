module.exports = {
    isANumber(number) {
        return `${number} is not a number!`;
    },

    isAnEmail(email) {
        return `${email} is not a valid email!`;
    },

    isEmpty(value) {
        return `${value} is empty!`;
    },

    minMaxLength(string) {
        return `${string} does not match the required length!`;
    },

    isABoolean(boolean) {
        return `${boolean} is not a boolean!`;
    },

    isClean(string) {
        return `${string} is not a valid string`;
    }
};