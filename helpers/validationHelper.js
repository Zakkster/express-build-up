module.exports = {
    isANumber(number) {
        return !isNaN(parseInt(number));
    },

    isAnEmail(email) {
        return /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(email);
    },

    isEmpty(value) {
        if (Array.isArray(value)) return value.length === 0;
        if (typeof value === 'object') return Object.keys(value).length === 0 && value.constructor === Object;

        return value.length === 0;
    },

    minMaxLength(string, min, max = false) {
        const isValid = string.length > min;
        if (max) return isValid && string.length < max;

        return isValid;
    },

    isABoolean(boolean) {
        return /^(true|false)$/.test(boolean);
    },

    isClean(string) {
        return /^([a-z0-9 ._-]+)$/i.test(string);
    }
};