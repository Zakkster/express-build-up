module.exports = class ErrorHelper {
    constructor(colors) {
        this.colors = colors;
    }

    displayError(error) {
        return console.log(this.colors.red.bold(error));
    }

    displayWarning(warning) {
        return console.log(this.colors.yellow.bold(warning));
    }

    displayInfo(info) {
        return console.log(this.colors.blue.bold(info));
    }
};