const express = require('express');
const session = require('express-session');
const morgan = require('morgan');
const helmet = require('helmet');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const mongoose = require('mongoose');

const User = require('./models/mongo/schemas/UserSchema');

module.exports = class App {
    constructor() {
        this.container = require('./di/registerClasses');

        this.dbConnector = this.container.get('dbConnector');
        this.config = this.container.get('mainConfig');
        this.auth = this.container.get('auth');

        this.passport = this.auth.passport;
        this.port = this.config.port;

        this.express = express();
        this.initializeMongo();
    }

    async start() {
        this.express.listen(this.port, () => {
            console.log('Express listening on port ' + this.port);
        });
    }

    initialize() {
        this.auth.initialize();
        this.configureMiddleware();
    }

    initializeMongo() {
        mongoose.connect('mongodb://localhost/test');

        const db = mongoose.connection;

        db.on('error', console.error.bind(console, 'connection error:'));

        db.once('open', function() {
            console.log('Mongo conn initialized!')
        });
    }

    configureMiddleware() {
        //secure Express apps with various HTTP headers
        this.express.use(helmet());

        //logger:
        this.express.use(morgan('dev'));

        this.express.use(express.static('public'));
        this.express.use(cookieParser());
        this.express.use(bodyParser.urlencoded({extended: true}));
        this.express.use(bodyParser.json());
        //session secret and cookie secret change
        this.express.use(session({
                secret: 'keyboard cat',
                resave: true,
                saveUninitialized: true
            })
        );

        this.express.use(this.passport.initialize());
        this.express.use(this.passport.session());

        //routes:
        this.express.use(require('./routes'));
    }
};