require('dotenv').config();

const path = require('path');

const CleanWebpackPlugin = require('clean-webpack-plugin');
const NodeExternals = require('webpack-node-externals');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const MinifyPlugin = require("babel-minify-webpack-plugin");

let devToolsOption = 'cheap-module-source-map';
let hintsOption = false;

if (process.env.NODE_ENV === 'production') {
    devToolsOption = 'none';
    hintsOption = 'warning';
}

/*const fs = require('fs');
const nodeModules = {};

fs.readdirSync('node_modules')
    .filter(x => ['.bin'].indexOf(x) === -1)
    .forEach(mod => nodeModules[mod] = 'commonjs ' + mod);*/

module.exports = {
    entry: ["babel-polyfill", "./index.js"],
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    //watch: true,
    devtool: devToolsOption,
    performance: {
        hints: hintsOption
    },
    target: 'node',
    externals: [NodeExternals()],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['es2015'],
                        "plugins": [
                            ["transform-regenerator", {
                                "asyncGenerators": false,
                                "generators": false,
                                "async": false
                            }]
                        ]
                    }
                }
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(['dist'], {verbose: true}),
        new MinifyPlugin({}, {
            test: /\.js($|\?)/i
        }),
        //new BundleAnalyzerPlugin(),
    ]
};