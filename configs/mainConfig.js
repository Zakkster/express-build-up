/**
 * Main config file for backend functionality
 */
module.exports = {
    port: 8000,
    saltRounds: 10,

    sessionSecret: '$2a$10$iH8DIvRKkIMoxFgW6JiFh.',
    cookieSecret: '$2a$10$GBylXFZHMfU5fHrOrTEdfe'
};