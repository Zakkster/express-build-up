require('dotenv').config();

module.exports = {
    connectionLimit: process.env.DB_CONN_LIMIT || 10,
    host: process.env.DB_HOST || 'localhost',
    user: process.env.DB_USER || 'root',
    password: process.env.DB_PASSWORD || '',
    database: process.env.DATABASE || 'test'
};