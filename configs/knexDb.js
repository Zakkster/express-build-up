require('dotenv').config();

module.exports = {
    client: 'mysql',
    connection: {
        connectionLimit: process.env.DB_CONN_LIMIT || 10,
        host: process.env.DB_HOST || 'localhost',
        user: process.env.DB_USER || 'root',
        password: process.env.DB_PASSWORD || '',
        database: process.env.DATABASE || 'test',
        charset: 'utf8_general_ci'
    },
    migrations: {
        tableName: 'migrations'
    }
};